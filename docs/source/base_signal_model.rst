:github_url: https://gitlab.com/e.ribeirosabidussi/emcqmri

Signal Module
=============
.. currentmodule:: EMCqMRI.core.base.base_signal_model.SignalModel

.. autofunction:: forward
.. autofunction:: initialize_parameters
