# Installation guide

EMCqMRI's core functionality is written in Python 3 (>= 3.6).

The package is currently distributed via Gitlab as the primary source code repository,
and the Python package index (PyPI).

This page provides steps to:
- [Preparation of development environment](#Preparing-the-development-enviroment)
- [Install Pytorch](#Installing-PyTorch)
- [Install EMCqMRI from PyPI](#Installing-the-EMCqMRI-package-with-PIP)
- [Validate the installation](#validating-the-installation)

---
### Preparing the development enviroment

- Create a virtual environment for the project: `python3 -m venv venv_example_project`

- Activate the virtual environment: `. venv_example_project/bin/activate`

- Upgrade pip and install wheel: `pip3 install --upgrade pip wheel` (this will make sure you have no errors and warnings during installations with pip)

### Installing PyTorch

- MacOS without CUDA support: `pip3 install torch torchvision`

- MacOS with CUDA support: follow instructions at this URL: https://github.com/pytorch/pytorch#from-source

- Linux with CUDA support: `pip3 install torch torchvision`

- Windows with CUDA support: `pip3 install torch==1.7.1 torchvision==0.8.2 -f https://download.pytorch.org/whl/torch_stable.html`

### Installing the EMCqMRI package with PIP

- After activating your virtual enviroment, run: `pip3 install EMCqMRI`.

### Validating the installation

Once installed, you can verify if everything is running correctly.
- Start a Python session in your terminal: `python3`
- Import the EMCqMRI package: `import EMCqMRI`
- Verify the package version: `print(EMCqMRI.__version__)`
