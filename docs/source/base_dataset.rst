:github_url: https://gitlab.com/e.ribeirosabidussi/emcqmri

Dataset Module
===============
.. currentmodule:: EMCqMRI.core.base.base_dataset.Dataset


.. autofunction:: index_files
.. autofunction:: set_data_path
.. autofunction:: load_folder
.. autofunction:: load_file
.. autofunction:: get_length
.. autofunction:: get_label
.. autofunction:: get_signal