import torch
from torch.nn.modules.loss import _Loss
from torch.nn import MSELoss

class MultiClassMSELoss(_Loss):
    def __init__(self, config_object):
        super(MultiClassMSELoss, self).__init__()
        self._loss = MSELoss()

    def forward(self, estimates, labels):
        if isinstance(estimates, list):
            loss = []
            for x_, y_ in zip(estimates, labels):
                loss.append(self._loss(x_, y_))
            loss_val = loss.mean()
            return loss_val            
        else:
            loss_val = self._loss(estimates,labels)
            return loss_val
