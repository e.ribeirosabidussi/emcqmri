base/base_dataset.py
    The following methods were incorporated within the base class:
    - def set_data_path(self, path):
        Assigns path to attribute self.path
    - def index_files(self):
        Stores the names of all files in self.args.engine.path as a list in the attribute self.filename_list
    - def load_folder(self):
        It loads all files in self.filename_list. The list attribute self.data contains all data loaded
    - def load_file(self, idx):
        Loads specific file in self.filename_list, indicated by the index **idx**. The attribute self.data is now a list with 1 element.
    - def get_length(self):
        Returns the number of files present in self.filename_list
        
    
engine/data_loader.py
    - The class SampleDataset now receives the configObj as input;
    - Data pre-loading is now handled by this class, by calling the method dataset.load_folder();
    - filename now is correct for all file types;

engine/estimate.py
    - Change data saving routine;
    - Data shape now should be handled by user: no squeezing happens;
    - Include changes for different types of labels: multi-task methods, with disjoint labels can now be used. If using multi-task (e.g. relaxometry + motion) Labels must now be a list with each type of label;

engine/train_model.py
    - Modularize script: create methods for handling batched data, and loss_logging;
    - Include handling for multi-task learning (labels as list);

modules/dataset/relaxometry/relaxometry_dataset.py
    - Clean up - Most methods for data loading is now handled by the parent class base_dataset

modules/inference_model/rim.py
    - Adapt the RIM for multi-task learning (labels as list);
    - Should not affect backward compatibility;

modules/likelihood_model/gaussian/gaussian.py
    - Adapt the Gaussian model for multi-task learning (labels as list);

utilities/core_utilities.py
    - Improve get_batch() to handle batches of lists (multi-task learning);

